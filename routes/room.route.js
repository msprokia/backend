const express = require('express');
const app = express();
const roomRoute = express.Router();

let RoomModel = require('../model/Room');

// Add room
roomRoute.route('/create-room').post((req, res, next) => {
 RoomModel.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get all room
roomRoute.route('/get-room-list').get((req, res) => {
  RoomModel.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single room
roomRoute.route('/get-room/:id').get((req, res) => {
  RoomModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update room
roomRoute.route('/update-room/:id').put((req, res, next) => {
  RoomModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Room successfully updated!')
    }
  })
})

// Delete room
roomRoute.route('/delete-room/:id').delete((req, res, next) => {
  RoomModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = roomRoute;