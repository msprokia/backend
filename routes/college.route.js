const express = require('express');
const app = express();
const collegeRoute = express.Router();

let CollegeModel = require('../model/College');

// Add college
collegeRoute.route('/create-college').post((req, res, next) => {
  CollegeModel.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get all college
collegeRoute.route('/get-college-list').get((req, res) => {
  CollegeModel.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single college
collegeRoute.route('/get-college/:id').get((req, res) => {
  CollegeModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update college
collegeRoute.route('/update-college/:id').put((req, res, next) => {
  CollegeModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('College successfully updated!')
    }
  })
})

// Delete college
collegeRoute.route('/delete-college/:id').delete((req, res, next) => {
  CollegeModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = collegeRoute;