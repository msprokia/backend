const express = require('express');
const app = express();
const blockRoute = express.Router();

let BlockModel = require('../model/Block');

// Add block
blockRoute.route('/create-block').post((req, res, next) => {
 BlockModel.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get all block
blockRoute.route('/get-block-list').get((req, res) => {
  BlockModel.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single block
blockRoute.route('/get-block/:id').get((req, res) => {
  BlockModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update block
blockRoute.route('/update-block/:id').put((req, res, next) => {
  BlockModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Block successfully updated!')
    }
  })
})

// Delete block
blockRoute.route('/delete-block/:id').delete((req, res, next) => {
  BlockModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = blockRoute;