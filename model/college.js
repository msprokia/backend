const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let College = new Schema({
  college_name: {
    type: String,
    required: true,
    validate:{
        validator: function(text){
            return text.length > 0;
        },
        message: "Empty name is not allowed"
     }
  }
})

module.exports = mongoose.model('College', College)