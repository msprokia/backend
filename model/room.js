const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Room = new Schema({
  room_no: {
    type: Number,
    required: true
  },
  block_id:{
    type:Schema.Types.ObjectId,
    ref:'Block'
  },
  roomtype:{
    required: true,
    type: String,
},
    rentalrate:{
    required: true,
    type: Number
},
    student:{
    type: String
}
})

module.exports = mongoose.model('Room', Room)