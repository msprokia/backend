const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Block = new Schema({
  block_name: {
    type: String,
    required: true,
    validate:{
        validator: function(text){
            return text.length > 0;
        },
        message: "Empty name is not allowed"
     }
  },
  college_id:{
    type:Schema.Types.ObjectId,
    ref:'College'
  }
})

module.exports = mongoose.model('Block',  Block)